using DataFrames, CSV, Printf, Dates


"""
function getMean(filename::String)::Array{Float64,2} 

## Description
    Import data from a CSV file and calculate the column means. 

## Input
   filename: String. Path and filename of the CSV file. 

## Output
    mdf: Array{Float64, 1}. Vector contianing the column means. 
"""
function getMean(filename::String)::Array{Float64,2} 
    df = DataFrame(CSV.File(filename, header=1))
    ddf = describe(df)
    mdf = Matrix(select(ddf, :mean))
    return mdf
end

"""
function generateLaTeX(author::String, data::Array{Float64, 2}, templatefile:: String, filename::String)

## Description
    Generate a Pre-defined LaTeX file based on a template and compile the file. 

## Input
   dict: Dictionary containing the tags and the values.
   templatefile: String. Path and filename of the template file. 
   filename: String. Path and filename of the output LaTeX file. 
"""
function generateLaTeX(dict::Dict{String,String}, templatefile:: String, filename::String)
    f = open(templatefile)
    s = read(f, String)
    close(f)
    for k in keys(dict)
	s = replace(s, k=>dict[k]) 
    end
    open(filename,"w") do f
	write(f, s)
    end
    command = `pdflatex $filename`
    pdfname = split(filename, ".")[1]*".pdf"
    opencommand = `open $pdfname`
    run(command)
    run(opencommand)
    
end

aut = "Felix Chan"
templatefile = "template.tex"
fname = "temp-"*Dates.format(Dates.now(), "yyyy-mm-ddTHH-MM-SS")*".tex"
mv = getMean("./data/rand3.csv")
n = size(mv)[1]
datatext = [@sprintf "%.4f" i for i in mv]
datatextalt = [string(round(i, digits=4)) for i in mv] # This is an alternative without macro. 
meantext = join(datatext, ", ", " and ") 
dict = Dict("{n}"=>string(n), "{author}"=>aut, "{meantext}"=>meantext)
generateLaTeX(dict, templatefile, fname)
