# ResBaz - My Introduction to Julia

## Introduction

This repo contains files and codes that form part of my presentation on [Julia](https://julialang.org/) in ResBaz 2021 and 2022 hosted by [Curtin University](https://www.curtin.edu.au/) Perth, WA.


## Files 

    1. ResBaz2021-Julia.slides.html - The reveal slides for presentation in 2021. Speed test results are based on Intel i7 Quad core with Python 3.7. 
    2. ResBaz2021-Julia.ipynb - The Jupyter notebook where ResBaz2021-Julia.slides.html was generated from. 
    3. ResBaz2021-Julia.html - The HTML version of ResBaz2021-Julia.ipynb
    4. ResBaz2021-Julia.css - Custom CSS file for the HTML file above. 
    5. helloworld.jl - Hello World code in Julia for demonstration. 
    6. logistic.jl - Julia code for generating the bifurcation data. For demonstration. 
    7. logistic.py - Python code for generating the bifurcation data. For demonstration. 
    8. Curtin.png - Curtin logo to be used with ResBaz2021-Julia.css
    9. ResBaz-Julia.ipynb - The latest Jupyter notebook with some updated test reuslts based on M1, Python 3.9 and Julia 1.8
    10. ResBaz-Julia.html - The HTML version of the ResBaz-Julia.ipynb. 
    11. ResBaz-Julia.slides.html - Reveal slides based on ResBaz-Julia.ipynb (somewhat ugly but can't be bothered fixing up the css).   

## Information 

The Jupyter notebook is written with the Python kernel and required [PyJulia](https://github.com/JuliaPy/pyjulia) to imlement the Julia magic. [IJulia](https://github.com/JuliaLang/IJulia.jl) and [PyCall](https://github.com/JuliaPy/PyCall.jl) should also be installed through Julia as Pycall is required for PyJulia to work.

## Other Directories
    1. Data - contains CSV files for demonstration. 
    2. pics - folder containing image files for the slides. 

## Note:
The convert function in Julia no longer able to convert a dataframe to a matrix. As such the getMean function now uses the Matrix function instead of Convert. 
