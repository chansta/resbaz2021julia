using DataFrames, CSV, Printf, Dates


"""
function getMean(filename::String)::Array{Float64,2} 

## Description
    Import data from a CSV file and calculate the column means. 

## Input
   filename: String. Path and filename of the CSV file. 

## Output
    mdf: Array{Float64, 1}. Vector contianing the column means. 
"""
function getMean(filename::String)::Array{Float64,2} 
    df = DataFrame(CSV.File(filename, header=1))
    ddf = describe(df)
    mdf = Matrix(select(ddf, :mean))
    return mdf
end

"""
function generateLaTeX(author::String, data::Array{Float64, 2}, templatefile:: String, filename::String)

## Description
    Generate a Pre-defined LaTeX file based on a template and compile the file. 

## Input
   author: String. Text to replace \$author variable in the template. 
   data: Array{Float64,1}. Vector that contains the column means. 
   templatefile: String. Path and filename of the template file. 
   filename: String. Path and filename of the output LaTeX file. 
"""
function generateLaTeX(author::String, data::Array{Float64, 2}, templatefile:: String, filename::String)
    n = size(data)[1]
    datatext = [@sprintf "%.4f" i for i in data]
    meantext = join(datatext, ", ", " and ") 
    f = open(templatefile)
    s = read(f, String)
    close(f)
    s = replace(s, r"{author}"=>author)
    s=replace(s, r"{n}"=>string(n))
    s=replace(s, r"{meantext}"=>meantext)
    open(filename,"w") do f
	write(f, s)
    end
    command = `pdflatex $filename`
    pdfname = split(filename, ".")[1]*".pdf"
    opencommand = `open $pdfname`
    run(command)
    run(opencommand)
    
end

aut = "Felix Chan"
templatefile = "template.tex"
fname = "temp-"*Dates.format(Dates.now(), "yyyy-mm-ddTHH-MM-SS")*".tex"
mv = getMean("./data/rand3.csv")
generateLaTeX(aut, mv, templatefile, fname)
